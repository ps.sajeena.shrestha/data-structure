package com.example.datastructure.service.queue;

import com.example.datastructure.DataStructureApplicationTests;
import com.example.datastructure.service.queue.impl.DynamicQueue;
import com.example.datastructure.service.queue.impl.FixedQueue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FixedQueueTest extends DataStructureApplicationTests {

    private Queue<Integer> queue;

    @Test
    @DisplayName("Given queue of size One, when enqueue two times throws exception(")
    public void givenTwoElementOnQueueSizeOne_whenEnqueue_thenThrowException() {
        queue = new FixedQueue<>(1);
        queue.enqueue(5);

        assertThrows(IllegalStateException.class, () -> queue.enqueue(5),
                "It should throw IllegalStateException");
    }

    @Test
    @DisplayName("Given three elements on queue, when dequeue returns first element and removes it")
    public void givenThreeElementOnQueue_whenDequeue_thenReturnFirstElementAndRemovesIt() {
        queue = new DynamicQueue<>();
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(8);

        assertEquals(4, queue.dequeue(), "Deleted element in queue should not be four");
        assertEquals(2, queue.size(), "Size of the queue should be two");
    }

    @Test
    @DisplayName("Given empty queue, when dequeue throws exception")
    public void givenEmptyQueue_whenDequeue_thenThrowException() {
        queue = new FixedQueue<>(0);

        assertThrows(IllegalStateException.class, () -> queue.dequeue(),
                "It should throw IllegalStateException");
    }

    @Test
    @DisplayName("Given two elements on queue, when get size returns size two")
    public void givenTwoElementOnQueue_whenGetSize_thenReturnSizeTwo() {
        queue = new FixedQueue<>(2);
        queue.enqueue(1);
        queue.enqueue(2);

        assertEquals(2, queue.size(), "Size of the queue should be two");
    }


    @Test
    @DisplayName("Given empty queue, when get size returns size zero")
    public void givenEmptyQueue_whenGetSize_thenReturnSizeZero() {
        queue = new FixedQueue<>(0);

        assertEquals(0, queue.size(), "Size of the queue should be 0");
    }

    @Test
    @DisplayName("Given two elements on queue, when peek returns first element without removing it")
    public void givenTwoElementOnQueue_whenPeek_thenReturnFirstElementWithoutRemovingIt() {
        queue = new FixedQueue<>(2);
        queue.enqueue(2);
        queue.enqueue(3);

        assertEquals(2, queue.peek(), "Element at the top of queue should be two");
        assertEquals(2, queue.size(), "Size of the queue should be two");
    }

    @Test
    @DisplayName("Given empty queue, when peek throws exception")
    public void givenEmptyQueue_whenPeek_thenThrowException() {
        queue = new FixedQueue<>(1);

        assertThrows(IllegalStateException.class, () -> queue.peek(), "It should throw IllegalStateException");
    }


}
