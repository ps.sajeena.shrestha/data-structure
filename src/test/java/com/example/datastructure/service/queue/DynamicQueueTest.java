package com.example.datastructure.service.queue;

import com.example.datastructure.DataStructureApplicationTests;
import com.example.datastructure.service.queue.impl.DynamicQueue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DynamicQueueTest extends DataStructureApplicationTests {

    private Queue<Integer> queue;

    @BeforeEach
    public void setUp() {
        queue = new DynamicQueue<>();
    }

    @Test
    @DisplayName("Given ten elements on queue of default size ten, when enqueue should not throw any exception")
    public void givenTenElementOnQueueDefaultSizeTen_whenEnqueue_thenShouldNotThrowException() {
        defaultTenTestData();

        assertDoesNotThrow(() -> queue.enqueue(11), "It should not throw any exceptions");
    }

    @Test
    @DisplayName("Given three elements on queue, when dequeue returns first element and removes it")
    public void givenThreeElementOnQueue_whenDequeue_thenReturnFirstElementAndRemovesIt() {
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(8);

        assertEquals(4, queue.dequeue(), "Deleted element in queue should be four");
        assertEquals(2, queue.size(), "Size of the queue should be two");
    }

    @Test
    @DisplayName("Given empty queue, when dequeue throws exception")
    public void givenEmptyQueue_whenDequeue_thenThrowException() {

        assertThrows(IllegalStateException.class, () -> queue.dequeue(),
                "It should throw IllegalStateException");
    }

    @Test
    @DisplayName("Given two elements on queue, when get size returns size two")
    public void givenTwoElementOnQueue_whenGetSize_thenReturnSizeTwo() {
        queue.enqueue(1);
        queue.enqueue(2);

        assertEquals(2, queue.size(), "Size of the queue should be two");
    }

    @Test
    @DisplayName("Given empty queue, when get size returns size zero")
    public void givenEmptyQueue_whenGetSize_thenReturnSizeZero() {

        assertEquals(0, queue.size(), "Size of the queue should be 0");
    }

    @Test
    @DisplayName("Given two elements on queue, when peek returns first element without removing it")
    public void givenTwoElementOnQueue_whenPeek_thenReturnFirstElementWithoutRemovingIt() {
        queue.enqueue(2);
        queue.enqueue(3);

        assertEquals(2, queue.peek(), "Element at the head of queue should be two");
        assertEquals(2, queue.size(), "Size of the queue should be two");
    }

    @Test
    @DisplayName("Given empty queue, when peek throws exception")
    public void givenEmptyQueue_whenPeek_thenThrowException() {

        assertThrows(IllegalStateException.class, () -> queue.peek(), "It should throw IllegalStateException");
    }

    private void defaultTenTestData() {
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        queue.enqueue(8);
        queue.enqueue(9);
        queue.enqueue(10);
    }

}
