package com.example.datastructure.service.stack;

import com.example.datastructure.service.stack.impl.FixedStack;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FixedStackTest {

    private Stack<Integer> stack;

    @Test
    @DisplayName("Given stack of size one, when push two times throws Exception")
    public void givenStackSizeOne_whenPushTwoElement_thenThrowException() {
        stack = new FixedStack<>(1);
        stack.push(1);

        assertThrows(IllegalStateException.class, () -> stack.push(2),
                "It should throw IllegalStateException");
    }

    @Test
    @DisplayName("Given three elements on stack, when pop returns last element and removes it")
    public void givenThreeElementOnStack_whenPop_thenReturnLastElementAndRemovesIt() {
        stack = new FixedStack<>(3);
        stack.push(4);
        stack.push(8);
        stack.push(12);

        assertEquals(12, stack.pop(), "Element pop from stack should be twelve");
        assertEquals(2, stack.size(), "Size of the stack should be two");

    }

    @Test
    @DisplayName("Given empty stack, when pop throws exception")
    public void givenEmptyStack_whenPop_thenThrowException() {
        stack = new FixedStack<>(0);

        assertThrows(IllegalStateException.class, () -> stack.pop(), "It should throw IllegalStateException");
    }

    @Test
    @DisplayName("Given two elements on stack, when get size returns two")
    public void givenTwoElementOnStack_whenGetSize_thenReturnTwo() {
        stack = new FixedStack<>(2);
        stack.push(1);
        stack.push(2);

        assertEquals(2, stack.size(), "Size of the stack should be two");
    }

    @Test
    @DisplayName("Given empty stack, when get size returns zero")
    public void givenEmptyStack_whenGetSize_thenReturnZero() {
        stack = new FixedStack<>(0);

        assertEquals(0, stack.size(), "Size of the stack should be 0");
    }

    @Test
    @DisplayName("Given two elements on stack, when peek returns last element without removing it")
    public void givenTwoElementOnStack_whenPeek_thenReturnLastElementWithoutRemovingIt() {
        stack = new FixedStack<>(2);
        stack.push(2);
        stack.push(3);

        assertEquals(3, stack.peek(), "Element at the top of stack should be three");
        assertEquals(2, stack.size(), "Size of the stack should be 2");
    }

    @Test
    @DisplayName("Given empty stack, when peek throws exception")
    public void givenEmptyStack_whenPeek_thenThrowException() {
        stack = new FixedStack<>(1);

        assertThrows(IllegalStateException.class, () -> stack.peek(), "It should throw IllegalStateException");
    }
}
