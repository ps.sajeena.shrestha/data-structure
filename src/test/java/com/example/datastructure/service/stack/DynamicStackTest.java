package com.example.datastructure.service.stack;

import com.example.datastructure.service.stack.impl.DynamicStack;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DynamicStackTest {

    private Stack<Integer> stack;

    @BeforeEach
    public void initialization() {
        stack = new DynamicStack<>();
    }

    @Test
    @DisplayName("Given ten elements on stack of default size ten, when push should not throw any exception")
    public void givenTenElementOnQueueDefaultSizeTen_whenPush_thenShouldNotThrowException() {
        defaultTenTestData();

        assertDoesNotThrow(() -> stack.push(11), "It should not throw any exceptions");
    }


    @Test
    @DisplayName("Given three elements on stack , when pop returns last element and removes it")
    public void givenThreeElementOnStack_whenPop_thenReturnLastElementAndRemovesIt() {
        stack.push(4);
        stack.push(8);
        stack.push(12);

        assertEquals(12, stack.pop(), "Element pop from stack should be twelve");
        assertEquals(2, stack.size(), "Size of the stack should be two");
    }

    @Test
    @DisplayName("Given empty stack, when pop throws exception")
    public void givenEmptyStack_whenPop_thenThrowException() {

        assertThrows(IllegalStateException.class, () -> stack.pop(), "It should throw IllegalStateException");
    }

    @Test
    @DisplayName("Given two elements on stack, when get size returns size two")
    public void givenTwoElementOnStack_whenGetSize_thenReturnSizeTwo() {
        stack.push(1);
        stack.push(2);

        assertEquals(2, stack.size(), "Size of the stack should be two");
    }

    @Test
    @DisplayName("Given empty stack, when get size returns size zero")
    public void givenEmptyStack_whenGetSize_thenReturnSizeZero() {

        assertEquals(0, stack.size(), "Size of the stack should be 0");
    }

    @Test
    @DisplayName("Given two elements on stack, when peek returns last element")
    public void givenTwoElementOnStack_whenPeek_thenReturnLastElement() {
        stack.push(2);
        stack.push(3);

        assertEquals(3, stack.peek(), "Element at the top of stack should be three");
    }

    @Test
    @DisplayName("Given empty stack, when peek throws exception")
    public void givenEmptyStack_whenPeek_thenThrowException() {

        assertThrows(IllegalStateException.class, () -> stack.peek(),
                "It should throw IllegalStateException");
    }

    private void defaultTenTestData() {
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.push(6);
        stack.push(7);
        stack.push(8);
        stack.push(9);
        stack.push(10);
    }
}
