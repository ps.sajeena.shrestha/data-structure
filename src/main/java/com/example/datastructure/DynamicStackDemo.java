package com.example.datastructure;

import com.example.datastructure.service.stack.Stack;
import com.example.datastructure.service.stack.impl.DynamicStack;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DynamicStackDemo {

    private static final Logger logger = LogManager.getLogger(DynamicStackDemo.class);

    public static void main(String[] args) {
        logger.info("Executing the dynamic stack");
        executeDynamicStackInteger();
        executeDynamicStackString();
    }

    private static void executeDynamicStackInteger() {
        Stack<Integer> stack = new DynamicStack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.push(6);
        stack.push(7);
        stack.push(8);
        stack.push(9);
        stack.push(10);
        logger.info("Current number of element in the stack : " + stack.size());
        logger.info("Element at the top of stack : " + stack.peek());

        stack.push(11);
        stack.push(12);
        stack.push(13);
        stack.push(14);
        stack.push(15);

        executePop(stack);
    }

    private static void executeDynamicStackString() {
        Stack<String> stack = new DynamicStack<>();
        stack.push("A");
        stack.push("B");
        stack.push("C");
        stack.push("D");
        stack.push("E");
        stack.push("F");
        stack.push("G");
        stack.push("H");
        stack.push("I");
        stack.push("J");

        logger.info("Current number of element in the stack : " + stack.size());
        logger.info("Element at the top of stack : " + stack.peek());

        stack.push("K");
        stack.push("L");
        stack.push("M");
        stack.push("N");
        stack.push("O");

        executePop(stack);
    }

    private static void executePop(Stack stack) {
        logger.info("Current number of element in the stack : " + stack.size());
        logger.info("Element at the top of stack : " + stack.peek());

        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();
        stack.pop();

        logger.info("Current number of element in the stack : " + stack.size());
        logger.info("Element at the top of stack : " + stack.peek());
    }

}
