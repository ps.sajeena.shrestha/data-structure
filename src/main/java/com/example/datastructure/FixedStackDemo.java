package com.example.datastructure;

import com.example.datastructure.service.stack.Stack;
import com.example.datastructure.service.stack.impl.FixedStack;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FixedStackDemo {

    private static final Logger logger = LogManager.getLogger(FixedStackDemo.class);

    public static void main(String[] args) {
        logger.info("Executing the fixed stack");
        executeFixedStackInteger();
        executeFixedStackString();
    }

    private static void executeFixedStackInteger() {
        Stack<Integer> stack = new FixedStack<>(3);
        stack.push(1);
        stack.push(2);
        stack.push(3);

        executePop(stack);

    }

    private static void executeFixedStackString() {
        Stack<String> stack = new FixedStack<>(3);
        stack.push("Apple");
        stack.push("Ball");
        stack.push("Cat");

        executePop(stack);

    }

    private static void executePop(Stack stack) {
        logger.info("Current number of element in the stack : " + stack.size());
        logger.info("Element at the top of stack : " + stack.peek());

        stack.pop();
        stack.pop();

        logger.info("Current number of element in the stack : " + stack.size());
        logger.info("Element at the top of stack : " + stack.peek());

        stack.pop();
        logger.info("Current number of element in the stack : " + stack.size());

    }

}
