package com.example.datastructure;

import com.example.datastructure.service.queue.Queue;
import com.example.datastructure.service.queue.impl.FixedQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FixedQueueDemo {

    private static final Logger logger = LogManager.getLogger(FixedQueueDemo.class);

    public static void main(String[] args) {
        logger.info("Executing the fixed queue");
        executeFixedQueueInteger();
        executeFixedQueueString();
    }

    private static void executeFixedQueueInteger() {
        Queue<Integer> queue = new FixedQueue(3);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);

        executeDequeue(queue);

    }

    private static void executeFixedQueueString() {
        Queue<String> queue = new FixedQueue(3);
        queue.enqueue("A");
        queue.enqueue("B");
        queue.enqueue("C");
        executeDequeue(queue);
    }

    private static void executeDequeue(Queue queue) {
        logger.info("Current number of element in the queue : " + queue.size());
        logger.info("Element at the top of fixed queue : " + queue.peek());

        queue.dequeue();
        queue.dequeue();

        logger.info("Current number of element in the queue : " + queue.size());
        logger.info("Element at the top of fixed queue : " + queue.peek());
    }

}
