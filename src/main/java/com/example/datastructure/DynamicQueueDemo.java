package com.example.datastructure;

import com.example.datastructure.service.queue.Queue;
import com.example.datastructure.service.queue.impl.DynamicQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DynamicQueueDemo {

    private static final Logger logger = LogManager.getLogger(DynamicQueueDemo.class);

    public static void main(String[] args) {
        logger.info("Executing the dynamic queue");
        executeDynamicQueueInteger();
        executeDynamicQueueString();
    }

    private static void executeDynamicQueueInteger() {
        logger.info("Executing the fixed queue");
        Queue<Integer> queue = new DynamicQueue<>();
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        queue.enqueue(8);
        queue.enqueue(9);
        queue.enqueue(10);
        logger.info("Current number of element in the queue : " + queue.size());
        logger.info("Element at the top of queue : " + queue.peek());

        queue.enqueue(11);
        queue.enqueue(12);
        queue.enqueue(13);
        queue.enqueue(14);
        queue.enqueue(15);

        executeDequeue(queue);
    }

    private static void executeDynamicQueueString() {
        Queue<String> queue = new DynamicQueue<>();
        queue.enqueue("A");
        queue.enqueue("B");
        queue.enqueue("C");
        queue.enqueue("D");
        queue.enqueue("E");
        queue.enqueue("F");
        queue.enqueue("G");
        queue.enqueue("H");
        queue.enqueue("I");
        queue.enqueue("J");

        logger.info("Current number of element in the queue : " + queue.size());
        logger.info("Element at the top of queue : " + queue.peek());

        queue.enqueue("K");
        queue.enqueue("L");
        queue.enqueue("M");
        queue.enqueue("N");
        queue.enqueue("O");

        executeDequeue(queue);
    }

    private static void executeDequeue(Queue queue) {
        logger.info("Current number of element in the queue : " + queue.size());
        logger.info("Element at the top of fixed queue : " + queue.peek());

        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();

        logger.info("Current number of element in the queue : " + queue.size());
        logger.info("Element at the top of fixed queue : " + queue.peek());
    }

}
