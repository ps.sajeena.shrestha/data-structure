package com.example.datastructure.constants;

public class MessageConstant {

    public static final String FULL_STACK = "Stack is full.";
    public static final String EMPTY_STACK = "Stack is empty.";
    public static final String PUSHED_INTO_STACK = "Pushed element into stack : {}";
    public static final String POP_FROM_STACK = "Pop element from stack : {}";
    public static final String EXPANDING_STACK = "Expanding stack.";
    public static final String SHRINKING_STACK = "Shrinking stack.";

    public static final String FULL_QUEUE = "Queue is full.";
    public static final String EMPTY_QUEUE = "Queue is empty.";
    public static final String INSERTED_INTO_QUEUE = "Inserted element into queue : {}";
    public static final String DELETED_FROM_QUEUE = "Deleted element from queue : {}";
    public static final String EXPANDING_QUEUE = "Expanding queue.";
    public static final String SHRINKING_QUEUE = " Shrinking queue.";

}
