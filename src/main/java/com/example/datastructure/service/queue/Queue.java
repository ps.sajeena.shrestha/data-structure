package com.example.datastructure.service.queue;

public interface Queue<T> {

    void enqueue(T element);

    T dequeue();

    int size();

    T peek();

}
