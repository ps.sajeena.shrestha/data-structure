package com.example.datastructure.service.queue.impl;

import com.example.datastructure.constants.MessageConstant;
import com.example.datastructure.service.queue.Queue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DynamicQueue<T> implements Queue<T> {

    private static final Logger logger = LogManager.getLogger(DynamicQueue.class);

    private static final int DEFAULT_CAPACITY = 10;

    private T[] array;
    private int head;
    private int tail;
    private int currentSize = 0;

    public DynamicQueue() {
        this(DEFAULT_CAPACITY);
    }

    public DynamicQueue(int initialSize) {
        resetQueueOnEmpty();
        //noinspection unchecked
        this.array = ((T[]) new Object[initialSize]);
    }

    @Override
    public void enqueue(T element) {
        if (isQueueFull()) {
            logger.info(MessageConstant.FULL_QUEUE);
            increaseSize();
        }

        addElement(element);
    }

    @Override
    public T dequeue() {
        T element = peek();

        shrink();
        logger.info(MessageConstant.DELETED_FROM_QUEUE, element);
        return element;
    }

    @Override
    public int size() {
        return currentSize;
    }

    @Override
    public T peek() {
        if (isQueueEmpty()) {
            logger.info(MessageConstant.EMPTY_QUEUE);
            resetQueueOnEmpty();
            throw new IllegalStateException("Queue is empty");
        } else {
            return array[head];
        }
    }

    private void resetQueueOnEmpty() {
        this.head = -1;
        this.tail = -1;
    }

    protected void addElement(T element) {
        if (head == -1) {
            head = 0;
        }

        tail++;
        currentSize++;
        array[tail] = element;
        logger.info(MessageConstant.INSERTED_INTO_QUEUE, element);
    }

    protected void removeElement(int size) {
        if (head >= tail)
            resetQueueOnEmpty();
        else {
            head++;
        }

        currentSize--;
        copyArray(size);
    }

    private boolean isQueueFull() {
        return currentSize == array.length;
    }

    private boolean isQueueEmpty() {
        return this.size() == 0;
    }

    private void increaseSize() {
        logger.info(MessageConstant.EXPANDING_QUEUE);
        copyArray(this.array.length << 1);
    }

    private void shrink() {
        if (currentSize < (this.array.length >> 2)) {
            logger.info(MessageConstant.SHRINKING_QUEUE);
            removeElement(array.length >> 1);
        } else {
            removeElement(array.length);
        }
    }

    private void copyArray(int newSize) {
        @SuppressWarnings("unchecked")
        T[] newArray = (T[]) new Object[newSize];
        int tempFront = head;

        for (int i = 0; i < tail; i++) {
            newArray[i] = this.array[tempFront];
            tempFront++;
        }

        this.array = newArray;
        head = 0;
        tail--;
    }

}
