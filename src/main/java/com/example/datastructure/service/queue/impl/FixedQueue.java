package com.example.datastructure.service.queue.impl;

import com.example.datastructure.constants.MessageConstant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FixedQueue<T> extends DynamicQueue<T> {

    private static final Logger logger = LogManager.getLogger(FixedQueue.class);
    private final int maxSize;

    public FixedQueue(int initialSize) {
        super(initialSize);
        this.maxSize = initialSize;
    }

    @Override
    public void enqueue(T element) {
        if (this.maxSize == size()) {
            logger.info(MessageConstant.FULL_QUEUE);
            throw new IllegalStateException("Queue is full");
        }

        super.enqueue(element);
    }

    @Override
    public T dequeue() {
        T element = peek();

        removeElement(this.maxSize);
        logger.info(MessageConstant.DELETED_FROM_QUEUE, element);
        return element;
    }


}
