package com.example.datastructure.service.stack.impl;

import com.example.datastructure.constants.MessageConstant;
import com.example.datastructure.service.stack.Stack;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DynamicStack<T> implements Stack<T> {

    private static final Logger logger = LogManager.getLogger(DynamicStack.class);

    private static final int DEFAULT_CAPACITY = 10;

    private int top;
    private T[] array;

    public DynamicStack(int initialSize) {
        //noinspection unchecked
        this.array = (T[]) new Object[initialSize];
        this.top = -1;
    }

    public DynamicStack() {
        this(DEFAULT_CAPACITY);
    }

    @Override
    public void push(T element) {
        this.addElement(element);
    }

    @Override
    public T pop() {
        T topElement = peek();
        logger.info(MessageConstant.POP_FROM_STACK, topElement);

        this.shrink();
        return topElement;
    }

    @Override
    public int size() {
        return top + 1;
    }

    @Override
    public T peek() {
        if (isStackEmpty()) {
            logger.info(MessageConstant.EMPTY_STACK);
            throw new IllegalStateException("Stack is empty");
        } else {
            return array[top];
        }
    }

    private void addElement(T element) {
        if (isStackFull()) {
            logger.info(MessageConstant.FULL_STACK);
            increaseSize();
        }

        logger.info(MessageConstant.PUSHED_INTO_STACK, element);
        this.array[++top] = element;
    }

    private void increaseSize() {
        logger.info(MessageConstant.EXPANDING_STACK);
        copyArray(array.length << 1);
    }

    private void shrink() {
        if (top < (array.length >> 2)) {
            logger.info(MessageConstant.SHRINKING_STACK);
            removeElement(array.length >> 1);
        }

        removeElement(array.length);
    }

    protected void removeElement(int size) {
        --top;
        copyArray(size);
    }

    private boolean isStackFull() {
        return size() == array.length;
    }

    private boolean isStackEmpty() {
        return this.size() == 0;
    }

    private void copyArray(int newSize) {
        @SuppressWarnings("unchecked")
        T[] newArray = (T[]) new Object[newSize];
        int count = size();

        if (count >= 0) {
            System.arraycopy(this.array, 0, newArray, 0, count);
        }

        this.array = newArray;
    }

}
