package com.example.datastructure.service.stack;

public interface Stack<T> {

    void push(T element);

    T pop();

    int size();

    T peek();

}
