package com.example.datastructure.service.stack.impl;

import com.example.datastructure.constants.MessageConstant;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FixedStack<T> extends DynamicStack<T> {

    private static final Logger logger = LogManager.getLogger(FixedStack.class);

    private final int maxSize;

    public FixedStack(int initialSize) {
        super(initialSize);
        this.maxSize = initialSize;
    }

    @Override
    public void push(T element) {
        if (this.maxSize == size()) {
            logger.info(MessageConstant.FULL_STACK);
            throw new IllegalStateException("Stack is full");
        }

        super.push(element);
    }

    @Override
    public T pop() {
        T topElement = peek();
        logger.info(MessageConstant.POP_FROM_STACK, topElement);

        removeElement(this.maxSize);
        return topElement;
    }

}
